var dest = require('gulp-dest');
var gulp = require('gulp');
var sftp = require('gulp-sftp');
var $    = require('gulp-load-plugins')();
var browserify = require('browserify');
var uglify = require('gulp-uglify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require( 'gulp-util' );
var ftp = require( 'vinyl-ftp' );
var jade = require('gulp-jade');




var sassPaths = [
  'node_modules/foundation-sites/scss',
  'node_modules/motion-ui/src'
];

gulp.task('sass', function() {
  return gulp.src('scss/app.scss')
    .pipe($.sass({
      includePaths: sassPaths
    })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(gulp.dest('dist'));
})


gulp.task('browserify', function() {

  var stream =browserify('js/app.js')
    .bundle()
    .pipe(source('app.bundle.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest('dist'));

    stream.on('error', function() {});
  return stream;
});


gulp.task('twig', function() {
var YOUR_LOCALS = {};
  return gulp.src('views/*.jade')
    .pipe(jade({
      locals: YOUR_LOCALS,
      pretty: true
    }))
    .pipe(dest('build/views', {ext: '.twig'}))
    .pipe(gulp.dest('./'))
});




gulp.task('default', ['sass', 'browserify', 'twig'], function() {
  gulp.watch(['scss/**/*.scss'], ['sass']);
  gulp.watch(['js/**/*.js'], [
    'browserify',
    ]
  );
  gulp.watch(['views/**/*.jade'], ['twig']);
});

  
