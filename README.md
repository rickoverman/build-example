


# The Stack

  - foundation6 & sass
  - browserify & uglify
  - Jade
  - gulp



## Commands

    npm run sass
    npm run browserify
    npm run twig

To start building and watching your js, scss, jade files use:

    npm start
