/**
 * App.js Main javascript file for the `Let It Grow` front-end.
 * @author Rick Overman
 */

// load console.log utility
log = require('./modules/log.js')


// Load configuration
var config = require('./../config/app.json');

// var $ = jQuery = require('jquery');
//
// // ajaxify/history.js
// ajaxify = require('ajaxify');


var foundation = require('foundation-sites');


var MobileDetect = require('mobile-detect'),
    md = new MobileDetect(window.navigator.userAgent);

// display console log header
log.head();


// log debug status
console.log('debug', config.debug);


// initialize the foundation frameworl
$(document).foundation();


// log file load
console.log('Loaded app.js');
